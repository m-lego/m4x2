4x2 in lego
===============

  ![M8 4x2](pics/m4x2.jpg)

this is a test board.... it uses:

- 2 x 74hc595 to scan the columns
- 1 x 74hc589 to scan the rows
- weact eink 2.13 screen, bw/bwr
- clickable/replacable encoder
- seeduino rp2040 *but any rp2040 or stm32 supported by qmk will do*
- it runs qmk, not in the develop yet.

pcb

![M8 4x2](pics/m4x2-pcb.png)

3d model

![M8 4x2](pics/m4x2-3d.png)
